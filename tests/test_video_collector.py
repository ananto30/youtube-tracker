import pytest

from app.data.youtube_search_data import YoutubeChannelVideos
from app.data.youtube_video_data import YoutubeVideo
from app.web_app import create_app
from tests.test_data import YOUTUBE_API_CHANNEL, YOUTUBE_API_VIDEO


@pytest.fixture
def app():
    app = create_app()
    yield app
    app.container.unwire()


def test__collect_channel_videos(requests_mock, app):
    channel_id = "123"
    youtube_api_key = app.container.cfg().youtube_api_key

    requests_mock.get(
        f"https://youtube.googleapis.com/youtube/v3/search?part=id&channelId={channel_id}&type=video&maxResults=50&key={youtube_api_key}",
        json=YOUTUBE_API_CHANNEL,
    )

    resp = app.container.video_collector_service().collect_channel_videos(channel_id)
    assert isinstance(resp, YoutubeChannelVideos)
    assert resp.pageInfo.totalResults == 91
    assert resp.items[0].id.videoId == "pKO9UjSeLew"
    assert resp.items[1].id.videoId == "uHt01D6rOLI"


def test__collect_channel_videos_with_page_token(requests_mock, app):
    channel_id = "123"
    youtube_api_key = app.container.cfg().youtube_api_key
    page_token = "def"

    requests_mock.get(
        f"https://youtube.googleapis.com/youtube/v3/search?pageToken={page_token}&part=id&channelId={channel_id}&type=video&maxResults=50&key={youtube_api_key}",
        json=YOUTUBE_API_CHANNEL,
    )

    resp = (
        app.container.video_collector_service().collect_channel_videos_with_page_token(
            channel_id, page_token
        )
    )
    assert isinstance(resp, YoutubeChannelVideos)
    assert resp.pageInfo.totalResults == 91
    assert resp.items[0].id.videoId == "pKO9UjSeLew"
    assert resp.items[1].id.videoId == "uHt01D6rOLI"


def test__collect_video_list_by_id_list(requests_mock, app):
    video_id = "YfXLLt-yqQo"
    youtube_api_key = app.container.cfg().youtube_api_key

    requests_mock.get(
        f"https://youtube.googleapis.com/youtube/v3/videos?part=snippet%2Cstatistics&id={video_id}&key={youtube_api_key}",
        json=YOUTUBE_API_VIDEO,
    )

    resp: YoutubeVideo = (
        app.container.video_collector_service().collect_video_list_by_id_list(
            [video_id]
        )
    )
    assert isinstance(resp, YoutubeVideo)
    assert resp.items[0].id == video_id
    assert resp.items[0].statistics.viewCount == "3240016"
