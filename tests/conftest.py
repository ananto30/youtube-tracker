import pytest
from fakeredis import FakeStrictRedis
from rq import Queue

from app.db.base import Database


@pytest.fixture(scope="session")
def memory_db():
    db = Database("sqlite:///:memory:")
    db.create_database()
    return db


@pytest.fixture(scope="session")
def queue_mock():
    queue = Queue(is_async=False, connection=FakeStrictRedis())
    return queue
