YOUTUBE_API_VIDEO = {
    "kind": "youtube#videoListResponse",
    "etag": "LblYYJaTYGKQ-E2ox3WYD7DwV5I",
    "items": [
        {
            "kind": "youtube#video",
            "etag": "qyHlmfIf1OJzbiVQKdtoeqaRXTE",
            "id": "YfXLLt-yqQo",
            "snippet": {
                "publishedAt": "2021-05-28T04:00:00Z",
                "channelId": "UCmBA_wu8xGg1OfOkfW13Q0Q",
                "title": "BAD BUNNY, LUAR LA L - 100 MILLONES [Visualizer]",
                "description": "BAD BUNNY, LUAR LA L\n100 MILLONES  [Visualizer]\nhttps://rimas.io/100MILLONES\n\nMerch: https://shop.elultimotourdelmundo.com/\u200b\n\nSuscríbete al canal: https://rimas.io/ytbadbunny\u200b\n\nMás de Bad Bunny:\nEUTDM - https://rimas.io/EUTDM\u200b\nUna Vez ft. Mora - https://youtu.be/gSMYGP0TYC4\u200b\n\nSigue a Bad Bunny:\nSpotify: https://rimas.io/spotifybb\u200b\nApple Music: https://rimas.io/applemusicbb\u200b\nInstagram: https://www.instagram.com/badbunnypr\u200b\n\nLetra:\n[Intro: Bad Bunny]\n¡Huh!\nYeah-eh, yeah\nEy\nYeh, ey (Grr), eh (Huh), eh (Grr)\n\n[Coro: Bad Bunny]\nCien millone' dando vuelta' en un Can-Am (Un Can-Am)\nY la' corta' ya tú sabe' dónde van (Prr, prr)\nUn saludo pa' lo hater', ¿cómo están? (Wuju)\nQue me odien, eso e' parte de mi plan, yeah (Ey, ey)\nMala mía, cabrone', pero estoy crecío' (Crecío'), eh, eh, eh\nSiempre con un piquete nuevo y el de ello' vencío' (Ey)\n\n[Verso 1: Bad Bunny]\nYa no e' Simón el que dice, ahora soy yo el que digo\nTu coro quiere hacer coro conmigo (Ey)\nMe la' clavo de tre' en tre' como si yo fuera lo' Migo' (Huh-huh)\nMere, mamón, tú y yo no somo' amigo' (No, no), ey\nAquí no hay que aparentar (Nah)\nAndo con unas teni' que tú no puede' cachar (Ajá)\nConcentra'o haciendo ticket me metí siete Adderall\nQuién e' el número uno no hay ni que preguntar (Ey, ey, ey)\nTo' saben que soy yo, eh (Wuh)\nDe PR pa'l mundo, gracia' al que creyó (Gracia')\nSentiste la presión cuando el camión te arrolló (Grr)\nSi llaman a su pai, lo vo'a coger yo (Yo, yo, yo)\nY si le' va mal, no e' culpa mía (No)\nEh-eh-eh-eh, cabrone', no e' culpa mía\nYo estoy en mi peak, yo estoy en la mía, eh (Mía, ah-ah-ah)\nYo estoy en la mía\n\n\n[Coro: Bad Bunny]\nCien millone' dando vuelta' en un Can-Am (Can-Am)\nY la' corta' ya tú sabe' dónde van (Saben ya)\nUn saludo pa' lo hater', ¿cómo están? (Hola)\nQue me odien, eso e' parte de mi plan, yeah (Huh, huh, huh, whuh)\nMala mía, cabrone', pero estoy crecío' (Crecío'), ey, eh, ey\nSiempre con un piquete nuevo y el de ello' vencío' (Ey, ey, ey)\n\n[Interludio: Luar La L]\n¿Qué? Sí, jeje, El Letra (Letra)\nMera, El Conejo me envió una jodienda ahí pa' que tirara (Ahí dale)\nAhora está en el estudio esperando\nPrende la mierda esa, vámono' pa'l carajo (Prende, prende)\nLos G4, cabrón (¿Qué?; ¿cómo?)\n\n[Verso 2: Luar La L]\nEn PR lo' carrito' y en Miami son la' Lambo\nLlegó el rookie que le brincó to' lo' rango (Jaja)\nY ahora to' quieren sacarla, yo ando con Bad Bu pichando (¿Qué pasó, cabrón?)\nDosciento' mil en prenda', hasta de día estoy brillando (Je)\nEn Can-Am por to'a la playa dando vuelta' (Woh)\nCuando me parqueé vi que tu doña capeó el flow de abrir la' puerta' (Habla claro)\nSalimo' con esta y cambió la vuelta, ¿qué? (¿Qué pasó?)\nY si van a bookearme, son má' de treinta (Sabe', sabe')\nEn carrito o en Corvetta (Jeje)\nToíto' quieren rolearme y ninguno pide la reta (¿Quién?)\nTengo un Role' y ni miro el tiempo, ando ruleta (Caro)\nTo' esto' hater' me lo maman, son peseta (¿Ah, qué?)\nTienen do' cara', esta combi sí e' cara (Lo e')\nSi fantasmea', dispara y les chillamo' en la cara\nSon peseta' do' cara' (Je), to'a mi' babie' son cara'\nSi la' mando disparan, rrr, y te chillamo' en la cara (¿Qué?)\n\n[Coro: Luar La L]\nCien millone' dando vuelta' en un Can-Am (Ah-ah-am)\nY la' pieza' ya tú sabe' dónde van\nUn saludo pa' lo hater' que son fan\nQue me odien, eso e' parte de mi plan, yeah (Yeah-eh-eh-eh)\nMala mía, cabrone', pero estoy crecío', yeah\nLes pasé a doscienta' milla', se quedaron dormío', yeah\n\n[Verso 3: Bad Bunny]\nMera, papi, ¿cuándo me va' a montar en el carrito?\nEy, me enamora' to' lo' mese' (To' lo' mese')\nEy, una baby quiere plata, otra quiere que la bese (Muah)\nCada cual tiene lo que merece\nChequéate lo' Billboard, llevo número uno doce mese' (Yeah, ey, ey)\nAuch, auch, eso duele (Eh)\nPa' to' el que me critica (Ja), que me lo pele (Rrr)\nToma un billete 'e cien pa' que sepa' cómo huele, huele (Huelebicho)\nY ahora tengo a to' lo' gringo' conspirando (Uh)\nPorque saben que el bori lo' 'tá matando, ey\nSuck my dick, motherfucker (Eh), eh\nLo' tengo mamando, eh\nAhora to' el mundo 'tá llamando (¿Ah?)\nYa mismo me retiro, no sé cuándo (No sé)\nLo' escupo como Triple H entrando (Nah)\nDando vuelta' en PR o en Orlando, eh-eh (¿Qué fue?)\n\n#BADBUNNY\u200b #100Millones\nBad,Bunny,Luar,La,L,100 Millones,El,Ultimo,Tour,Del,Mundo\n\n\n© 2021 Rimas Entertainment",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/YfXLLt-yqQo/default.jpg",
                        "width": 120,
                        "height": 90,
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/YfXLLt-yqQo/mqdefault.jpg",
                        "width": 320,
                        "height": 180,
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/YfXLLt-yqQo/hqdefault.jpg",
                        "width": 480,
                        "height": 360,
                    },
                    "standard": {
                        "url": "https://i.ytimg.com/vi/YfXLLt-yqQo/sddefault.jpg",
                        "width": 640,
                        "height": 480,
                    },
                    "maxres": {
                        "url": "https://i.ytimg.com/vi/YfXLLt-yqQo/maxresdefault.jpg",
                        "width": 1280,
                        "height": 720,
                    },
                },
                "channelTitle": "Bad Bunny",
                "tags": [
                    "Bad",
                    "Bunny",
                    "Bad Bunny",
                    "YHLQMDLG",
                    "100 millones",
                    "bad bunny 100 millones",
                    "bad bunny 100",
                    "bad bunny luar",
                    "luar la l",
                    "bad bunny que me odien",
                ],
                "categoryId": "10",
                "liveBroadcastContent": "none",
                "localized": {
                    "title": "BAD BUNNY, LUAR LA L - 100 MILLONES [Visualizer]",
                    "description": "BAD BUNNY, LUAR LA L\n100 MILLONES  [Visualizer]\nhttps://rimas.io/100MILLONES\n\nMerch: https://shop.elultimotourdelmundo.com/\u200b\n\nSuscríbete al canal: https://rimas.io/ytbadbunny\u200b\n\nMás de Bad Bunny:\nEUTDM - https://rimas.io/EUTDM\u200b\nUna Vez ft. Mora - https://youtu.be/gSMYGP0TYC4\u200b\n\nSigue a Bad Bunny:\nSpotify: https://rimas.io/spotifybb\u200b\nApple Music: https://rimas.io/applemusicbb\u200b\nInstagram: https://www.instagram.com/badbunnypr\u200b\n\nLetra:\n[Intro: Bad Bunny]\n¡Huh!\nYeah-eh, yeah\nEy\nYeh, ey (Grr), eh (Huh), eh (Grr)\n\n[Coro: Bad Bunny]\nCien millone' dando vuelta' en un Can-Am (Un Can-Am)\nY la' corta' ya tú sabe' dónde van (Prr, prr)\nUn saludo pa' lo hater', ¿cómo están? (Wuju)\nQue me odien, eso e' parte de mi plan, yeah (Ey, ey)\nMala mía, cabrone', pero estoy crecío' (Crecío'), eh, eh, eh\nSiempre con un piquete nuevo y el de ello' vencío' (Ey)\n\n[Verso 1: Bad Bunny]\nYa no e' Simón el que dice, ahora soy yo el que digo\nTu coro quiere hacer coro conmigo (Ey)\nMe la' clavo de tre' en tre' como si yo fuera lo' Migo' (Huh-huh)\nMere, mamón, tú y yo no somo' amigo' (No, no), ey\nAquí no hay que aparentar (Nah)\nAndo con unas teni' que tú no puede' cachar (Ajá)\nConcentra'o haciendo ticket me metí siete Adderall\nQuién e' el número uno no hay ni que preguntar (Ey, ey, ey)\nTo' saben que soy yo, eh (Wuh)\nDe PR pa'l mundo, gracia' al que creyó (Gracia')\nSentiste la presión cuando el camión te arrolló (Grr)\nSi llaman a su pai, lo vo'a coger yo (Yo, yo, yo)\nY si le' va mal, no e' culpa mía (No)\nEh-eh-eh-eh, cabrone', no e' culpa mía\nYo estoy en mi peak, yo estoy en la mía, eh (Mía, ah-ah-ah)\nYo estoy en la mía\n\n\n[Coro: Bad Bunny]\nCien millone' dando vuelta' en un Can-Am (Can-Am)\nY la' corta' ya tú sabe' dónde van (Saben ya)\nUn saludo pa' lo hater', ¿cómo están? (Hola)\nQue me odien, eso e' parte de mi plan, yeah (Huh, huh, huh, whuh)\nMala mía, cabrone', pero estoy crecío' (Crecío'), ey, eh, ey\nSiempre con un piquete nuevo y el de ello' vencío' (Ey, ey, ey)\n\n[Interludio: Luar La L]\n¿Qué? Sí, jeje, El Letra (Letra)\nMera, El Conejo me envió una jodienda ahí pa' que tirara (Ahí dale)\nAhora está en el estudio esperando\nPrende la mierda esa, vámono' pa'l carajo (Prende, prende)\nLos G4, cabrón (¿Qué?; ¿cómo?)\n\n[Verso 2: Luar La L]\nEn PR lo' carrito' y en Miami son la' Lambo\nLlegó el rookie que le brincó to' lo' rango (Jaja)\nY ahora to' quieren sacarla, yo ando con Bad Bu pichando (¿Qué pasó, cabrón?)\nDosciento' mil en prenda', hasta de día estoy brillando (Je)\nEn Can-Am por to'a la playa dando vuelta' (Woh)\nCuando me parqueé vi que tu doña capeó el flow de abrir la' puerta' (Habla claro)\nSalimo' con esta y cambió la vuelta, ¿qué? (¿Qué pasó?)\nY si van a bookearme, son má' de treinta (Sabe', sabe')\nEn carrito o en Corvetta (Jeje)\nToíto' quieren rolearme y ninguno pide la reta (¿Quién?)\nTengo un Role' y ni miro el tiempo, ando ruleta (Caro)\nTo' esto' hater' me lo maman, son peseta (¿Ah, qué?)\nTienen do' cara', esta combi sí e' cara (Lo e')\nSi fantasmea', dispara y les chillamo' en la cara\nSon peseta' do' cara' (Je), to'a mi' babie' son cara'\nSi la' mando disparan, rrr, y te chillamo' en la cara (¿Qué?)\n\n[Coro: Luar La L]\nCien millone' dando vuelta' en un Can-Am (Ah-ah-am)\nY la' pieza' ya tú sabe' dónde van\nUn saludo pa' lo hater' que son fan\nQue me odien, eso e' parte de mi plan, yeah (Yeah-eh-eh-eh)\nMala mía, cabrone', pero estoy crecío', yeah\nLes pasé a doscienta' milla', se quedaron dormío', yeah\n\n[Verso 3: Bad Bunny]\nMera, papi, ¿cuándo me va' a montar en el carrito?\nEy, me enamora' to' lo' mese' (To' lo' mese')\nEy, una baby quiere plata, otra quiere que la bese (Muah)\nCada cual tiene lo que merece\nChequéate lo' Billboard, llevo número uno doce mese' (Yeah, ey, ey)\nAuch, auch, eso duele (Eh)\nPa' to' el que me critica (Ja), que me lo pele (Rrr)\nToma un billete 'e cien pa' que sepa' cómo huele, huele (Huelebicho)\nY ahora tengo a to' lo' gringo' conspirando (Uh)\nPorque saben que el bori lo' 'tá matando, ey\nSuck my dick, motherfucker (Eh), eh\nLo' tengo mamando, eh\nAhora to' el mundo 'tá llamando (¿Ah?)\nYa mismo me retiro, no sé cuándo (No sé)\nLo' escupo como Triple H entrando (Nah)\nDando vuelta' en PR o en Orlando, eh-eh (¿Qué fue?)\n\n#BADBUNNY\u200b #100Millones\nBad,Bunny,Luar,La,L,100 Millones,El,Ultimo,Tour,Del,Mundo\n\n\n© 2021 Rimas Entertainment",
                },
                "defaultAudioLanguage": "es",
            },
            "statistics": {
                "viewCount": "3240016",
                "likeCount": "488363",
                "dislikeCount": "5453",
                "favoriteCount": "0",
                "commentCount": "32577",
            },
        }
    ],
    "pageInfo": {"totalResults": 1, "resultsPerPage": 1},
}
YOUTUBE_API_CHANNEL = {
    "kind": "youtube#searchListResponse",
    "etag": "A13oYWgkwy5-VbIbFcH8BJd8-so",
    "nextPageToken": "CAIQAA",
    "regionCode": "BD",
    "pageInfo": {"totalResults": 91, "resultsPerPage": 2},
    "items": [
        {
            "kind": "youtube#searchResult",
            "etag": "Q8csnJM_WchP4MBsjSg90Cn-d-0",
            "id": {"kind": "youtube#video", "videoId": "pKO9UjSeLew"},
        },
        {
            "kind": "youtube#searchResult",
            "etag": "KPnxSNO-vFGU0pm0Rzbn0Iu_g4I",
            "id": {"kind": "youtube#video", "videoId": "uHt01D6rOLI"},
        },
    ],
}
