from unittest import mock

import pytest
from redis import Redis

from app.data.youtube_search_data import (
    YoutubeChannelVideos,
    PageInfo,
    Item as SearchItem,
    Id,
)
from app.data.youtube_video_data import (
    YoutubeVideo,
    Item as VideoItem,
    Snippet,
    Statistics,
)
from app.model.video import Video, VideoTag, VideoStat, VideoPerformance
from app.service.video_collector_service import VideoCollectorService
from app.workers import rq_worker, stat_scheduler


############
# These are not perfect unittests, mixed of unittest and integration test to ensure actual scenarios
############


@pytest.fixture
def worker():
    worker = rq_worker
    yield worker
    worker.container.unwire()


@pytest.mark.run(order=1)
def test__collect_channel(worker, memory_db, queue_mock):
    redis_mock = mock.Mock(spec=Redis)
    collector_mock = mock.Mock(spec=VideoCollectorService)

    collector_mock.collect_channel_videos.return_value = YoutubeChannelVideos(
        nextPageToken="nextPage",
        pageInfo=PageInfo(totalResults=4, resultsPerPage=2),
        items=[
            SearchItem(id=Id(videoId="a1b2c3")),
            SearchItem(id=Id(videoId="d4e5f6")),
        ],
    )
    collector_mock.collect_channel_videos_with_page_token.return_value = (
        YoutubeChannelVideos(
            pageInfo=PageInfo(totalResults=4, resultsPerPage=2),
            items=[
                SearchItem(id=Id(videoId="asdf")),
                SearchItem(id=Id(videoId="zxcv")),
            ],
        )
    )
    collector_mock.collect_video_list_by_id_list.return_value = YoutubeVideo(
        items=[
            VideoItem(
                id="a1b2c3",
                snippet=Snippet(channelId="123", title="youtube"),
                statistics=Statistics(),
            ),
            VideoItem(
                id="d4e5f6",
                snippet=Snippet(
                    channelId="123", title="youtube", tags=["vlog", "data", "science"]
                ),
                statistics=Statistics(viewCount="666"),
            ),
            VideoItem(
                id="asdf",
                snippet=Snippet(channelId="123", title="youtube", tags=["ml"]),
                statistics=Statistics(viewCount="777"),
            ),
            VideoItem(
                id="zxcv",
                snippet=Snippet(
                    channelId="123", title="youtube", tags=["vlog", "engineer"]
                ),
                statistics=Statistics(viewCount="888"),
            ),
        ]
    )

    channel_id = "123"

    with worker.container.job_queue.override(queue_mock), worker.container.rds.override(
        redis_mock
    ), worker.container.video_collector_service.override(
        collector_mock
    ), worker.container.db.override(
        memory_db
    ):
        id, status = worker.create_channel_video_job(channel_id)

    assert isinstance(id, str)
    assert isinstance(status, str)
    assert status == "finished"

    with memory_db.session() as session:
        assert len(session.query(Video).all()) == 4
        videos = session.query(Video).all()
        assert videos[0].id == "a1b2c3"
        assert videos[1].id == "d4e5f6"
        assert videos[2].id == "asdf"
        assert videos[3].id == "zxcv"

        assert len(session.query(VideoTag).all()) == 6
        video_tag = session.query(VideoTag).all()
        assert video_tag[0].video_id == "d4e5f6"
        assert video_tag[1].video_id == "d4e5f6"
        assert video_tag[2].video_id == "d4e5f6"
        assert video_tag[3].video_id == "asdf"

        assert len(session.query(VideoStat).all()) == 4
        assert session.query(VideoStat).all()[0].video_id == "a1b2c3"
        assert session.query(VideoStat).all()[1].video_id == "d4e5f6"
        assert (
            session.query(VideoStat)
            .filter(VideoStat.video_id == "d4e5f6")
            .first()
            .view_count
            == 666
        )

        assert len(session.query(VideoPerformance).all()) == 4
        assert session.query(VideoPerformance).all()[0].video_id == "a1b2c3"
        assert session.query(VideoPerformance).all()[1].video_id == "d4e5f6"
        video_perf1 = (
            session.query(VideoPerformance)
            .filter(VideoPerformance.video_id == "a1b2c3")
            .first()
        )
        assert video_perf1.first_hour_views == 0
        assert video_perf1.first_view_count == 0
        assert video_perf1.last_view_count == 0
        video_perf2 = (
            session.query(VideoPerformance)
            .filter(VideoPerformance.video_id == "d4e5f6")
            .first()
        )
        assert video_perf2.first_hour_views == 0
        assert video_perf2.first_view_count == 666
        assert video_perf2.last_view_count == 666


@pytest.mark.run(order=2)
def test__create_stat_collection_scheduler(worker, memory_db, queue_mock):
    redis_mock = mock.Mock(spec=Redis)
    collector_mock = mock.Mock(spec=VideoCollectorService)

    collector_mock.collect_video_list_by_id_list.return_value = YoutubeVideo(
        items=[
            VideoItem(
                id="a1b2c3",
                snippet=Snippet(channelId="123", title="youtube"),
                statistics=Statistics(viewCount="100"),
            ),
            VideoItem(
                id="d4e5f6",
                snippet=Snippet(
                    channelId="123", title="youtube", tags=["vlog", "data", "science"]
                ),
                statistics=Statistics(viewCount="678"),
            ),
            VideoItem(
                id="asdf",
                snippet=Snippet(channelId="123", title="youtube", tags=["ml"]),
                statistics=Statistics(viewCount="900"),
            ),
            VideoItem(
                id="zxcv",
                snippet=Snippet(
                    channelId="123", title="youtube", tags=["vlog", "engineer"]
                ),
                statistics=Statistics(viewCount="889"),
            ),
        ]
    )

    with worker.container.job_queue.override(
        queue_mock
    ), worker.container.scheduler_job_queue.override(
        queue_mock
    ), worker.container.rds.override(
        redis_mock
    ), worker.container.video_collector_service.override(
        collector_mock
    ), worker.container.db.override(
        memory_db
    ):
        stat_scheduler.create_stat_collection_scheduler()
        # to test enqueue_in we have to wait for that time, rather just call it
        stat_scheduler._calculate_all_video_performance()

    with memory_db.session() as session:
        assert len(session.query(Video).all()) == 4
        videos = session.query(Video).all()
        assert videos[0].id == "a1b2c3"
        assert videos[1].id == "d4e5f6"
        assert videos[2].id == "asdf"
        assert videos[3].id == "zxcv"

        assert len(session.query(VideoStat).all()) == 8

        assert len(session.query(VideoPerformance).all()) == 4
        assert session.query(VideoPerformance).all()[0].video_id == "a1b2c3"
        assert session.query(VideoPerformance).all()[1].video_id == "d4e5f6"

        video_perf1 = (
            session.query(VideoPerformance)
            .filter(VideoPerformance.video_id == "a1b2c3")
            .first()
        )
        assert video_perf1.first_hour_views == 100 - 0
        assert video_perf1.first_view_count == 0
        assert video_perf1.last_view_count == 100
        assert video_perf1.first_hour_views_calculated is False
        assert video_perf1.performance == 100 / 56

        video_perf2 = (
            session.query(VideoPerformance)
            .filter(VideoPerformance.video_id == "d4e5f6")
            .first()
        )
        assert video_perf2.first_hour_views == 678 - 666
        assert video_perf2.first_view_count == 666
        assert video_perf2.last_view_count == 678
        assert video_perf2.first_hour_views_calculated is False
        assert video_perf2.performance == 12 / 56

        video_perf3 = (
            session.query(VideoPerformance)
            .filter(VideoPerformance.video_id == "asdf")
            .first()
        )
        assert video_perf3.first_hour_views == 900 - 777
        assert video_perf3.first_view_count == 777
        assert video_perf3.last_view_count == 900
        assert video_perf3.first_hour_views_calculated is False
        assert video_perf3.performance == 123 / 56
