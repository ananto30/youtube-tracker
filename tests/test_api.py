import pytest

from app.model.video import Video
from app.web_app import create_app


@pytest.fixture
def app():
    app = create_app()
    yield app
    app.container.unwire()


@pytest.mark.run(order=3)
def test__query_videos(client, app, memory_db):
    with memory_db.session() as session:
        assert len(session.query(Video).all()) == 4

    with app.container.db.override(memory_db):
        resp = client.get("/videos")

    assert resp.status_code == 200
    assert resp.json == {
        "videos": [
            {
                "id": "asdf",
                "performance": 2.1964285714285716,
                "tags": ["ml"],
                "title": "youtube",
                "view_count": "900",
            },
            {
                "id": "a1b2c3",
                "performance": 1.7857142857142858,
                "tags": [""],
                "title": "youtube",
                "view_count": "100",
            },
            {
                "id": "d4e5f6",
                "performance": 0.21428571428571427,
                "tags": ["vlog", "data", "science"],
                "title": "youtube",
                "view_count": "678",
            },
            {
                "id": "zxcv",
                "performance": 0.017857142857142856,
                "tags": ["vlog", "engineer"],
                "title": "youtube",
                "view_count": "889",
            },
        ]
    }


@pytest.mark.run(order=4)
def test__query_videos_tag(client, app, memory_db):
    with memory_db.session() as session:
        assert len(session.query(Video).all()) == 4

    with app.container.db.override(memory_db):
        resp = client.get("/videos?tag=data")

    assert resp.status_code == 200
    assert resp.json == {
        "videos": [
            {
                "id": "d4e5f6",
                "performance": 0.21428571428571427,
                "tags": ["vlog", "data", "science"],
                "title": "youtube",
                "view_count": "678",
            }
        ]
    }

    with app.container.db.override(memory_db):
        resp = client.get("/videos?tag=data,vlog")

    assert resp.status_code == 200
    assert resp.json == {
        "videos": [
            {
                "id": "d4e5f6",
                "performance": 0.21428571428571427,
                "tags": ["vlog", "data", "science"],
                "title": "youtube",
                "view_count": "678",
            },
            {
                "id": "zxcv",
                "performance": 0.017857142857142856,
                "tags": ["vlog", "engineer"],
                "title": "youtube",
                "view_count": "889",
            },
        ]
    }

    with app.container.db.override(memory_db):
        resp = client.get("/videos?tag=nothing")

    assert resp.status_code == 200
    assert resp.json == {"videos": []}


@pytest.mark.run(order=5)
def test__query_videos_performance(client, app, memory_db):
    with memory_db.session() as session:
        assert len(session.query(Video).all()) == 4

    with app.container.db.override(memory_db):
        resp = client.get("/videos?performance_gte=.2")

    assert resp.status_code == 200
    assert resp.json == {
        "videos": [
            {
                "id": "asdf",
                "performance": 2.1964285714285716,
                "tags": ["ml"],
                "title": "youtube",
                "view_count": "900",
            },
            {
                "id": "a1b2c3",
                "performance": 1.7857142857142858,
                "tags": [""],
                "title": "youtube",
                "view_count": "100",
            },
            {
                "id": "d4e5f6",
                "performance": 0.21428571428571427,
                "tags": ["vlog", "data", "science"],
                "title": "youtube",
                "view_count": "678",
            },
        ]
    }

    with app.container.db.override(memory_db):
        resp = client.get("/videos?performance_lte=2")

    assert resp.status_code == 200
    assert resp.json == {
        "videos": [
            {
                "id": "a1b2c3",
                "performance": 1.7857142857142858,
                "tags": [""],
                "title": "youtube",
                "view_count": "100",
            },
            {
                "id": "d4e5f6",
                "performance": 0.21428571428571427,
                "tags": ["vlog", "data", "science"],
                "title": "youtube",
                "view_count": "678",
            },
            {
                "id": "zxcv",
                "performance": 0.017857142857142856,
                "tags": ["vlog", "engineer"],
                "title": "youtube",
                "view_count": "889",
            },
        ]
    }

    with app.container.db.override(memory_db):
        resp = client.get("/videos?performance_gte=.2&performance_lte=2")

    assert resp.status_code == 200
    assert resp.json == {
        "videos": [
            {
                "id": "a1b2c3",
                "performance": 1.7857142857142858,
                "tags": [""],
                "title": "youtube",
                "view_count": "100",
            },
            {
                "id": "d4e5f6",
                "performance": 0.21428571428571427,
                "tags": ["vlog", "data", "science"],
                "title": "youtube",
                "view_count": "678",
            },
        ]
    }

    with app.container.db.override(memory_db):
        resp = client.get("/videos?performance_gte=2&performance_lte=2")

    assert resp.status_code == 200
    assert resp.json == {"videos": []}

    with app.container.db.override(memory_db):
        resp = client.get("/videos?performance_gte=2.5&performance_lte=2")

    print(resp.json)
    assert resp.status_code == 422


@pytest.mark.run(order=6)
def test__query_videos_tag_and_performance(client, app, memory_db):
    with memory_db.session() as session:
        assert len(session.query(Video).all()) == 4

    with app.container.db.override(memory_db):
        resp = client.get("/videos?tag=data&performance_gte=.2")

    assert resp.status_code == 200
    assert resp.json == {
        "videos": [
            {
                "id": "d4e5f6",
                "performance": 0.21428571428571427,
                "tags": ["vlog", "data", "science"],
                "title": "youtube",
                "view_count": "678",
            }
        ]
    }

    with app.container.db.override(memory_db):
        resp = client.get("/videos?tag=vlog,data&performance_lte=2")

    assert resp.status_code == 200
    assert resp.json == {
        "videos": [
            {
                "id": "d4e5f6",
                "performance": 0.21428571428571427,
                "tags": ["vlog", "data", "science"],
                "title": "youtube",
                "view_count": "678",
            },
            {
                "id": "zxcv",
                "performance": 0.017857142857142856,
                "tags": ["vlog", "engineer"],
                "title": "youtube",
                "view_count": "889",
            },
        ]
    }

    with app.container.db.override(memory_db):
        resp = client.get("/videos?tag=science&performance_gte=.2&performance_lte=2")

    assert resp.status_code == 200
    assert resp.json == {
        "videos": [
            {
                "id": "d4e5f6",
                "performance": 0.21428571428571427,
                "tags": ["vlog", "data", "science"],
                "title": "youtube",
                "view_count": "678",
            }
        ]
    }

    with app.container.db.override(memory_db):
        resp = client.get("/videos?tag=vlog&performance_gte=2.5&performance_lte=2")

    print(resp.json)
    assert resp.status_code == 422
