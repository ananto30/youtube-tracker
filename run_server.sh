#!/bin/bash
export $(grep -v '^#' .env | xargs)
gunicorn app.wsgi:app --workers 4 -b 0.0.0.0:$PORT --access-logfile "-" --log-file "-" --log-level info