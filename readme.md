# Youtube Tracker

This app collects video data, tracks progress and calculate performance.

Usable through Web API interface.

Stacks used:

- Language: Python
- Framework: Flask
- Database: MySQL
- ORM: SQLAlchemy
- Code formatter: black
- Data modeling and parser: Pydantic
- API specs: Redoc, Swagger
- Containerization: Docker
- Job queue: python-rq

The app is built with low coupling using `dependency-injector` package.

*Please navigate through the API docs for all APIs. APIs are not included in this readme.*

**Swagger**: `/apidoc/swagger`

**Redoc**: `/apidoc/redoc`

## Architecture

The app actually delegates works to the worker thus highly scalable. The idea is to create tasks/jobs from the main app and the workers will pick them up and do asynchronously.

Like if we want to collect videos from a channel, we will call the web API `/channel/collect` and the app will create a job and return the job id and status. Then the worker will pickup the task as
its convenience and call Youtube's API to collect data and save in DB.

### Scheduler

The scheduler collects data periodically. The interval can be set from `config.yml`.

We have to start the scheduler by doing a HTTP POST on `/scheduler/start`. We can check out what jobs are scheduled with a HTTP GET on `/scheduler`.

### Diagrams

- This is the architecture of how **channel video collection** works -

![ChannelWorker](readme_images/channel_worker.png)

- This is the architecture of how **scheduler** gets periodical data -

![StatScheduler](readme_images/stat_scheduler.png)

The hack is the worker actually publishing the same job with a schedule time. So this will create a chain or "recursiveness" and the stat collection job will be done periodically.

- This is how we can **scale** -

![Scaling](readme_images/scaling.png)

We can run N number of workers for parallelism. The workers themselves are concurrent with the power of `multiprocessing` of Python.

## Run

MUST use **Python 3.9+**

### Docker

The easiest way to run everything.

```
docker compose up --build
```

If you don't have the tables created then do this -

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python create_table.py
```

*Please consider that `create_table.py` is a naive script to create tables. See the script and make necessary changes.*

In production never do such things, rather make migrations with tools like alembic.

Now you can find the API doc in -

**Swagger**: http://localhost:8000/apidoc/swagger

**Redoc**: http://localhost:8000/apidoc/redoc

### Virtual env

The *Pythonic* way to run.

- Install dependencies

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

- Start Mysql, Redis

```bash
docker compose up mysql redis
```

- Start the worker

```bash
sh run_workers.sh
```

- Start the app

```bash
sh run_server.sh
```

*For **development** use this to run the app -*

```bash
python -m app
```

## Project structure

The `app` contains the main Flask app along with the workers.

- `api` - Web API endpoints.
- `container` - Dependency injector containers, used to decouple layers.
- `data` - Request, response classes and Youtube API's response models.
- `db` - Database connection manager with session context manager.
- `error` - Youtube tracker's own exception classes.
- `model` - Database models.
- `repository` - ORM wrapper for our usecases, (not traditional repository).
- `service` - Business layer of Flask app and video collection client.
- `workers` - Job workers and schedulers. Responsible to collect video and save in db.

## Run tests

- Install dependencies

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements-test.txt
```

- Run tests

```bash
python -m pytest --cov=./app
```

## Philosophy

We actually do the performance calculation proactively to increase query performance. As it is stated that there will be a lot of queries, this is done. There is a db models just to ensure faster
calculation of performance. Please go through the `app/model.py` folder.

We periodically collect stats using a scheduler. This scheduler should be started with an API call on `/scheduler/start`. Why? Because we want to have control on the scheduler not going out of
control. We can also check jobs by calling `/scheduler` and stop scheduling jobs using `/scheduler/stop`. We can either call it caveat or feature!

The track interval of video stats is configurable from `config.yml`. This solely depends on the business requirements. If we need to show near realtime updates to users we can reduce to 1 minute or
even lower. But there are always tardeoffs. There should be a balance between server cost and user satisfaction.

## Some caveats

We dont't actually get the first hour views using Youtube's API. So, if the video is uploaded before we started tracking or collecting the channel data we can never get the actual first hour views.
That's why we are calculating first hour views from when we have started tracking.

We do a running performance calculation, that we collect stats and do performance calculation after each batch of collection. In this process we can show performance of those videos which are not even
1 hour old. But it has a tradeoff, the performance calculation is not super perfect. As there can be more than 1 videos that are not older than 1 hours. So they all have a running performance which
won't be correct as the perfomance update is done by batch. Each batch can have a similar median but when the next batch gets updated the median can get changed.

The performance calculation is done by a worker slightly after collecting data. So it can happen that any given time the performance calculation has started but the data collection is not complete
yet. So we will get slightly wrong performance for that run. But in the next run it will be adjusted as we got the `last_view_count`. And eventually after some hours, the first hour calculation will
be done forever (as they are first hour only 😅) and the performance calculation should be close to accurate based on the data we have. Or we can just calculate performance after first hour data is
completed, in that case we could not show the running performance, like videos that are less than 1 hour old will have no performance. So it's a tradeoff.