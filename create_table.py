from sqlalchemy import create_engine

from app.db.base import Base


########################
# IMPORTANT: This file is focused on mysql
# And edit init as per needed
########################


def init_db(host, port, user, password, database):
    db_uri = f"mysql://{user}:{password}@{host}:{port}"
    engine = create_engine(db_uri)

    # connect to db
    conn = engine.connect()
    conn.execute("commit")

    # make db
    try:
        conn.execute(f"create database {database}")
    except Exception as e:
        print(e)

    # connect to db youtube
    db = f"{db_uri}/{database}"
    engine = create_engine(db)

    # this is important, or sqlalchemy wont know what table to make
    from app.model.video import (
        Video,
        VideoStat,
        VideoTag,
        VideoPerformance,
    )

    Video
    VideoStat
    VideoTag
    VideoPerformance

    Base.metadata.create_all(bind=engine)


if __name__ == "__main__":
    # EDIT AS PER NEEDED
    init_db(
        host="0.0.0.0", port=3306, user="root", password="example", database="youtube"
    )
    print("Successfully created tables.")
