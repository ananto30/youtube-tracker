from dataclasses import dataclass

from dependency_injector import containers, providers
from redis import Redis
from rq import Queue

from app.db.base import Database
from app.repository.video import VideoRepository
from app.service.video_collector_service import VideoCollectorService


@dataclass
class WorkerConfig:
    scheduler_interval: int
    youtube_api_key: str


class WorkerContainer(containers.DeclarativeContainer):
    config = providers.Configuration(strict=True)

    cfg = providers.Factory(WorkerConfig, scheduler_interval=config.scheduler.interval, youtube_api_key=config.youtube.api_key)

    db = providers.Singleton(Database, db_url=config.db.url, db_log=config.db.log)
    rds = providers.Singleton(Redis, host=config.redis.host, port=config.redis.port)
    job_queue = providers.Singleton(Queue, connection=rds)
    scheduler_job_queue = providers.Singleton(Queue, name="scheduler", connection=rds)

    video_repository = providers.Factory(
        VideoRepository,
        session=db.provided.session,
    )

    video_collector_service = providers.Factory(
        VideoCollectorService,
        youtube_api_key=config.youtube.api_key,
    )
