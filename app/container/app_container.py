from dataclasses import dataclass

from dependency_injector import containers, providers

from app.db.base import Database
from app.repository.video import VideoRepository
from app.service.video import VideoService
from app.service.video_collector_service import VideoCollectorService


@dataclass
class AppConfig:
    youtube_api_key: str


class AppContainer(containers.DeclarativeContainer):
    config = providers.Configuration(strict=True)

    cfg = providers.Factory(AppConfig, youtube_api_key=config.youtube.api_key)

    db = providers.Singleton(Database, db_url=config.db.url, db_log=config.db.log)

    video_repository = providers.Factory(
        VideoRepository,
        session=db.provided.session,
    )

    video_collector_service = providers.Factory(
        VideoCollectorService,
        youtube_api_key=config.youtube.api_key,
    )

    video_service = providers.Factory(VideoService, video_repository=video_repository)
