class ApplicationError(Exception):
    status_code = 500


class ValidationException(ApplicationError):
    status_code = 422


class NotAllowedException(ApplicationError):
    status_code = 422
