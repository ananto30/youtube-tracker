from typing import List, Optional

from pydantic import BaseModel


class JobResponse(BaseModel):
    job_id: str
    status: str


class Statistics(BaseModel):
    view_count: Optional[str]
    like_count: Optional[str]
    dislike_count: Optional[str]
    favorite_count: Optional[str]
    comment_count: Optional[str]


class VideoResponse(BaseModel):
    id: str
    title: str
    view_count: str
    tags: List[str]
    # updated_at: str
    performance: float


class VideoResponseList(BaseModel):
    videos: List[VideoResponse]


class ErrorResponse(BaseModel):
    err: str


class ScheduledJobs(BaseModel):
    jobs: List[JobResponse]
