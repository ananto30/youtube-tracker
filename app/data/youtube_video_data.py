from typing import List, Optional

from pydantic import BaseModel


class Snippet(BaseModel):
    channelId: str
    title: str
    description: Optional[str]
    channelTitle: Optional[str]
    tags: Optional[List[str]]


class Statistics(BaseModel):
    viewCount: Optional[str]
    likeCount: Optional[str]
    dislikeCount: Optional[str]
    favoriteCount: Optional[str]
    commentCount: Optional[str]


class Item(BaseModel):
    id: str
    snippet: Snippet
    statistics: Statistics


class YoutubeVideo(BaseModel):
    items: List[Item]
