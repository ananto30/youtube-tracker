from typing import Optional

from pydantic import BaseModel, validator, root_validator

from app.error.errors import ValidationException


class SaveChannelRequest(BaseModel):
    channel_id: str

    @validator("channel_id")
    def name_must_contain_space(cls, v):
        if v == "":
            raise ValidationException("channel_id cannot be blank")
        return v


class QueryVideo(BaseModel):
    tag: Optional[str]
    performance_gte: Optional[float]
    performance_lte: Optional[float]

    @root_validator
    def check_order_id(cls, values):
        if (
            values.get("performance_gte") and values.get("performance_lte")
        ) and values.get("performance_gte") > values.get("performance_lte"):
            raise ValidationException(
                "performance_lte can't be less than performance_gte"
            )
        return values


class SaveVideoRequest(BaseModel):
    video_id: str

    @validator("video_id")
    def name_must_contain_space(cls, v):
        if v == "":
            raise ValidationException("video_id cannot be blank")
        return v
