from typing import List, Optional

from pydantic import BaseModel


class PageInfo(BaseModel):
    totalResults: int
    resultsPerPage: int


class Id(BaseModel):
    videoId: str


class Item(BaseModel):
    id: Id


class YoutubeChannelVideos(BaseModel):
    pageInfo: PageInfo
    nextPageToken: Optional[str]
    items: List[Item]
