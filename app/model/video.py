import datetime
from typing import List

from sqlalchemy import Column, String, Integer, Float, ForeignKey, DateTime, Boolean
from sqlalchemy.ext.hybrid import hybrid_property

from app.db.base import Base


class Video(Base):
    __tablename__ = "video"

    id = Column(String(36), primary_key=True)  # uuid length
    title = Column(String(128))  # youtube allows 100
    description = Column(String(5000))  # youtube allows 5000
    channel_id = Column(String(36))  # uuid length
    channel_title = Column(String(128))  # youtube allows 60

    # denormalize to reduce joins or db calls, only for view purpose
    _tags = Column(
        "tags", String(1024), default="[]", server_default="[]"
    )  # may be a candidate for increasing size

    @hybrid_property
    def tags(self):
        return self._tags.split(",")

    @tags.setter
    def tags(self, tags: List[str]):
        self._tags = ",".join(tags) if tags else ""


class VideoTag(Base):
    __tablename__ = "video_tag"

    id = Column(Integer, primary_key=True)
    video_id = Column(String(36), ForeignKey(Video.id))
    tag = Column(String(512), index=True)  # youtube allows 400


class VideoStat(Base):
    """
    This table is saved for expandability.
    Like for now we calculate performance based on view count, may be in future we may change the formula and calcuate based on like count.
    So we can just iterate through the history and recalculate the performance again.
    It is kind of like a event store (in a sense we can go to any given state and recalculate again to reach current state).
    """

    __tablename__ = "video_stat"

    id = Column(Integer, primary_key=True)
    video_id = Column(String(36), ForeignKey(Video.id), index=True)
    view_count = Column(Integer)
    like_count = Column(Integer)
    dislike_count = Column(Integer)
    comment_count = Column(Integer)
    favorite_count = Column(Integer)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)


class VideoPerformance(Base):
    """
    This table only helps to generate performance in effective way.
    It denormalizes the database but saves huge computation during the query.
    """

    __tablename__ = "video_performance"

    video_id = Column(String(36), ForeignKey(Video.id), primary_key=True)
    channel_id = Column(String(36))
    first_hour_views = Column(Integer)
    first_hour_views_calculated = Column(
        Boolean
    )  # denormalize for faster lookups, we ignore calculating when this is true
    performance = Column(Float)

    first_view_count = Column(Integer)
    last_view_count = Column(Integer)
    tracking_started_at = Column(DateTime)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
