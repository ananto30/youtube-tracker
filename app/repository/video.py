import datetime
import logging
import statistics
from contextlib import AbstractContextManager
from typing import Callable, List

from sqlalchemy import func, asc, and_
from sqlalchemy.orm import Session

from app.data.youtube_video_data import Item, Statistics
from app.model.video import Video, VideoStat, VideoTag, VideoPerformance


class VideoRepository:
    def __init__(self, session: Callable[..., AbstractContextManager[Session]]) -> None:
        self.session = session

    def get_videos_by_tags(self, tags: List[str]) -> [(Video, int, float)]:
        with self.session() as session:
            return (
                session.query(
                    Video, func.max(VideoStat.view_count), VideoPerformance.performance
                )
                    .filter(
                    Video.id == VideoTag.video_id,
                    Video.id == VideoStat.video_id,
                    Video.id == VideoPerformance.video_id,
                )
                    .filter(VideoTag.tag.in_(tags))
                    .group_by(VideoStat.video_id)
                    .order_by(VideoPerformance.performance.desc())
                    .limit(50)
                    .all()
            )

    def get_videos_by_performance(self, performance_gte, performance_lte) -> List:
        with self.session() as session:
            conditions = []
            if performance_gte:
                conditions.append(VideoPerformance.performance >= performance_gte)
            if performance_lte:
                conditions.append(VideoPerformance.performance <= performance_lte)

            return (
                session.query(
                    Video, func.max(VideoStat.view_count), VideoPerformance.performance
                )
                    .filter(
                    Video.id == VideoStat.video_id,
                    Video.id == VideoPerformance.video_id,
                )
                    .filter(and_(*conditions))
                    .group_by(VideoStat.video_id)
                    .order_by(VideoPerformance.performance.desc())
                    .limit(50)
                    .all()
            )

    def get_videos_by_tags_and_performance(
            self, tags: List[str], performance_gte, performance_lte
    ) -> List:
        with self.session() as session:
            conditions = []
            if performance_gte:
                conditions.append(VideoPerformance.performance >= performance_gte)
            if performance_lte:
                conditions.append(VideoPerformance.performance <= performance_lte)

            return (
                session.query(
                    Video, func.max(VideoStat.view_count), VideoPerformance.performance
                )
                    .filter(
                    Video.id == VideoTag.video_id,
                    Video.id == VideoStat.video_id,
                    Video.id == VideoPerformance.video_id,
                )
                    .filter(and_(*conditions))
                    .group_by(VideoStat.video_id)
                    .filter(VideoTag.tag.in_(tags))
                    .order_by(VideoPerformance.performance.desc())
                    .limit(50)
                    .all()
            )

    def get_videos(self) -> List:
        with self.session() as session:
            return (
                session.query(
                    Video, func.max(VideoStat.view_count), VideoPerformance.performance
                )
                    .filter(
                    Video.id == VideoStat.video_id,
                    Video.id == VideoPerformance.video_id,
                )
                    .group_by(VideoStat.video_id)
                    .order_by(VideoPerformance.performance.desc())
                    .limit(50)
                    .all()
            )

    def get_all_video_ids(self) -> [(str,)]:
        with self.session() as session:
            return session.query(Video.id).all()

    def get_videos_of_incomplete_first_hour_calculations(
            self,
    ) -> List[VideoPerformance]:
        with self.session() as session:
            return (
                session.query(VideoPerformance)
                    .filter(VideoPerformance.first_hour_views_calculated == False)
                    .all()
            )

    def save_youtube_video_with_stats(self, youtube_video: Item):
        with self.session() as session:
            exists = (
                session.query(Video.id).filter(Video.id == youtube_video.id).first()
            )
            if exists:
                logging.warning(f"{youtube_video.id} already exists in db")
                return
            video = Video(
                id=youtube_video.id,
                title=youtube_video.snippet.title,
                description=youtube_video.snippet.description,
                channel_id=youtube_video.snippet.channelId,
                channel_title=youtube_video.snippet.channelTitle,
            )
            # treat as a property
            video.tags = youtube_video.snippet.tags

            session.add(video)

            video_stat = VideoStat(
                video_id=youtube_video.id,
                view_count=youtube_video.statistics.viewCount or 0,
                like_count=youtube_video.statistics.likeCount or 0,
                dislike_count=youtube_video.statistics.dislikeCount or 0,
                comment_count=youtube_video.statistics.commentCount or 0,
                favorite_count=youtube_video.statistics.favoriteCount or 0,
            )

            session.add(video_stat)

            if youtube_video.snippet.tags:
                for tag in youtube_video.snippet.tags:
                    video_tag = VideoTag(video_id=youtube_video.id, tag=tag)
                    session.add(video_tag)

            video_performance = VideoPerformance(
                video_id=youtube_video.id,
                channel_id=youtube_video.snippet.channelId,
                first_hour_views=0,
                first_hour_views_calculated=False,
                performance=0,
                first_view_count=youtube_video.statistics.viewCount or 0,
                last_view_count=youtube_video.statistics.viewCount or 0,
                tracking_started_at=datetime.datetime.utcnow(),
            )
            session.add(video_performance)

            try:
                session.commit()
            except Exception as e:
                session.rollback()
                logging.exception(e)
                raise e

    def save_video_stat(self, video_id: str, stats: Statistics):
        with self.session() as session:
            video_stat = VideoStat(
                video_id=video_id,
                view_count=stats.viewCount,
                like_count=stats.likeCount,
                dislike_count=stats.dislikeCount,
                comment_count=stats.commentCount,
                favorite_count=stats.favoriteCount,
            )
            session.add(video_stat)
            session.commit()

    def update_video_performance(self, video_id: str, view_count: int):
        """
        Proactively update video performance to help calculating video performance.
        """
        with self.session() as session:
            # get video if first hour calculation is not done
            video_perf: VideoPerformance = (
                session.query(VideoPerformance)
                    .filter(VideoPerformance.video_id == video_id)
                    .filter(VideoPerformance.first_hour_views_calculated == False)
                    .first()
            )
            if video_perf:
                now = datetime.datetime.utcnow()
                # do not update when it's passed 1 hour
                # the calculation is not done yet but 1 hour has passed - it can happen as the calculation is done by another worker
                duration = now - video_perf.tracking_started_at
                if duration.total_seconds() < 3600:
                    # update first hour views
                    video_perf.first_hour_views += view_count - video_perf.last_view_count
                    # update latest view count
                    video_perf.last_view_count = view_count

                    video_perf.updated_at = now

                    session.commit()

    def calculate_all_video_performance(self):
        """
        Proactively generate video performance to reduce query while fetching videos.

        This is the only fancy method in this repository. Could be moved to service.
        As we are using sqlalchemy orm, keeping here makes more sense.

        :return:
        """
        channel_median_cache = {}
        with self.session() as session:
            # get video if first hour calculation is not done
            video_perfs: [VideoPerformance] = (
                session.query(VideoPerformance)
                    .filter(VideoPerformance.first_hour_views_calculated == False)
                    .all()
            )
            for video_perf in video_perfs:

                if video_perf.channel_id in channel_median_cache:
                    median = channel_median_cache[video_perf.channel_id]
                else:
                    # get channel's first hour views in asc order
                    results = (
                        session.query(VideoPerformance.first_hour_views)
                            .filter(VideoPerformance.channel_id == video_perf.channel_id)
                            .order_by(asc(VideoPerformance.first_hour_views))
                            .all()
                    )
                    # convert tuple to list
                    channels_firs_hour_views = [r for r, in results]
                    # get channel's all videos median
                    median = statistics.median(channels_firs_hour_views)
                    channel_median_cache[video_perf.channel_id] = median

                # performance calculation
                video_perf.performance = (
                    (video_perf.first_hour_views / median) if median != 0 else 0
                )

                # mark first hour calculation done if tracking time was more than 3600 secs ago
                duration = video_perf.updated_at - video_perf.tracking_started_at
                if duration.total_seconds() >= 3600:
                    video_perf.first_hour_views_calculated = True

                session.commit()
