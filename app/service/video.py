from typing import List

from app.data.request_data import QueryVideo
from app.data.response_data import VideoResponse
from app.repository.video import VideoRepository


class VideoService:
    def __init__(self, video_repository: VideoRepository):
        self._video_repository = video_repository

    def get_videos(self, query: QueryVideo) -> List[VideoResponse]:
        tags = query.tag.split(",") if query.tag else [""]
        if query.tag and not query.performance_gte and not query.performance_lte:
            row = self._video_repository.get_videos_by_tags(tags)
        elif not query.tag and (query.performance_gte or query.performance_lte):
            row = self._video_repository.get_videos_by_performance(
                query.performance_gte, query.performance_lte
            )
        elif query.tag and (query.performance_gte or query.performance_lte):
            row = self._video_repository.get_videos_by_tags_and_performance(
                tags, query.performance_gte, query.performance_lte
            )
        else:
            row = self._video_repository.get_videos()

        return [
            VideoResponse(
                id=video.id,
                title=video.title,
                tags=video.tags,
                view_count=view_count,
                performance=performance,
            )
            for video, view_count, performance in row
        ]
