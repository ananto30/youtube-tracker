import logging
from typing import List

import requests
from pydantic import parse_obj_as

from app.error.errors import NotAllowedException
from app.data.youtube_search_data import YoutubeChannelVideos
from app.data.youtube_video_data import YoutubeVideo


class VideoCollectorService:
    def __init__(self, youtube_api_key: str) -> None:
        self.youtube_api_key = youtube_api_key

    def collect_channel_videos(self, channel_id: str) -> YoutubeChannelVideos:
        query_params = {
            "part": "id",
            "channelId": channel_id,
            "type": "video",
            "maxResults": 50,
            "key": self.youtube_api_key,
        }
        result = self._call_url(
            f"https://youtube.googleapis.com/youtube/v3/search",
            query_params=query_params,
        )
        resp = parse_obj_as(YoutubeChannelVideos, result)
        return resp

    def collect_channel_videos_with_page_token(
        self, channel_id: str, page_token: str
    ) -> YoutubeChannelVideos:
        query_params = {
            "pageToken": page_token,
            "part": "id",
            "channelId": channel_id,
            "type": "video",
            "maxResults": 50,
            "key": self.youtube_api_key,
        }
        result = self._call_url(
            f"https://youtube.googleapis.com/youtube/v3/search",
            query_params=query_params,
        )
        resp = parse_obj_as(YoutubeChannelVideos, result)
        return resp

    def collect_video_list_by_id_list(self, video_ids: List[str]) -> YoutubeVideo:
        if len(video_ids) > 50:
            raise NotAllowedException("api can't get more than 50 results")
        query_params = {
            "part": "snippet,statistics",
            "id": video_ids,
            "key": self.youtube_api_key,
        }
        result = self._call_url(
            f"https://youtube.googleapis.com/youtube/v3/videos",
            query_params=query_params,
        )
        resp = parse_obj_as(YoutubeVideo, result)
        logging.info(f"Successfully collected video data for videoId: {video_ids}")
        return resp

    def collect_video_by_id(self, video_id: str) -> YoutubeVideo:
        query_params = {
            "part": "snippet,statistics",
            "id": video_id,
            "key": self.youtube_api_key,
        }
        result = self._call_url(
            f"https://youtube.googleapis.com/youtube/v3/videos",
            query_params=query_params,
        )
        resp = parse_obj_as(YoutubeVideo, result)
        logging.info(f"Successfully collected video data for videoId: {video_id}")
        return resp

    def _call_url(self, url: str, query_params: dict = None) -> dict:
        result = requests.get(url, params=query_params)
        if result.status_code == 200:
            return result.json()
        else:
            raise requests.HTTPError(result.status_code, result.content)
