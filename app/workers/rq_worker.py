import logging
from typing import List

from rq import Queue

from app.container.worker_container import WorkerContainer
from app.repository.video import VideoRepository
from app.service.video_collector_service import VideoCollectorService

####################
# The rq worker actually uses multiprocess and in Python these processes are replicated with pickling the code.
# We cannot use class based approach here for this reason, as an instance cannot be pickled.
# Why? Cause an instance is a reference in the current process, cannot be shared in multiple processes.
# Maybe we could use ipc, but an overkill for this app.
####################


logger = logging.getLogger(__name__)
logging.basicConfig(
    format="%(asctime)s | %(process)d | %(module)s : %(message)s", level=logging.INFO
)

# retry_policy = Retry(max=3, interval=[10, 30, 60])
retry_policy = None

# create a new container in each process
container = WorkerContainer()
container.config.from_yaml("config.yml")


def create_channel_video_job(channel_id: str) -> (str, str):
    job_queue: Queue = container.job_queue()
    job = job_queue.enqueue(_collect_channel_videos, channel_id, retry=retry_policy)
    return job.get_id(), job.get_status()


def create_single_video_job(video_id: str) -> (str, str):
    job_queue: Queue = container.job_queue()
    job = job_queue.enqueue(_collect_single_video, video_id, retry=retry_policy)
    return job.get_id(), job.get_status()


def create_single_video_stat_collection_job(video_id: str) -> (str, str):
    job_queue: Queue = container.job_queue()
    job = job_queue.enqueue(_collect_single_video_stat, video_id, retry=retry_policy)
    return job.get_id(), job.get_status()


def _collect_channel_videos(channel_id: str):
    video_collector_service: VideoCollectorService = container.video_collector_service()
    job_queue: Queue = container.job_queue()

    result = video_collector_service.collect_channel_videos(channel_id)
    video_ids = [i.id.videoId for i in result.items]
    logger.info(f"Collected channel: {channel_id} video ids: {video_ids}")
    job_queue.enqueue(_collect_video_list, video_ids, retry=retry_policy)
    while result.nextPageToken:
        result = video_collector_service.collect_channel_videos_with_page_token(
            channel_id, result.nextPageToken
        )
        video_ids = [i.id.videoId for i in result.items]
        logger.info(f"Collected channel: {channel_id} video ids: {video_ids}")
        job_queue.enqueue(_collect_video_list, video_ids, retry=retry_policy)


def _collect_video_list(video_ids: List[str]):
    video_collector_service: VideoCollectorService = container.video_collector_service()
    video_repository: VideoRepository = container.video_repository()

    result = video_collector_service.collect_video_list_by_id_list(video_ids)
    for video in result.items:
        video_repository.save_youtube_video_with_stats(video)


def _collect_single_video(video_id: str):
    video_collector_service: VideoCollectorService = container.video_collector_service()
    video_repository: VideoRepository = container.video_repository()

    result = video_collector_service.collect_video_by_id(video_id)
    video = result.items[0]
    video_repository.save_youtube_video_with_stats(video)


def _collect_single_video_stat(video_id: str):
    video_collector_service: VideoCollectorService = container.video_collector_service()
    video_repository: VideoRepository = container.video_repository()

    result = video_collector_service.collect_video_by_id(video_id)
    video = result.items[0]
    video_repository.save_video_stat(video.id, video.statistics)
