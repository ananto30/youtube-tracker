import logging
from datetime import timedelta
from typing import List

from rq import Queue
from rq.job import Job
from rq.registry import ScheduledJobRegistry

from app.repository.video import VideoRepository
from app.service.video_collector_service import VideoCollectorService
from app.workers.rq_worker import container

####################
# The stat scheduler actually uses multiprocess and in Python these processes are replicated with pickling the code.
# We cannot use class based approach here for this reason, as an instance cannot be pickled.
# Why? Cause an instance is a reference in the current process, cannot be shared in multiple processes.
# Maybe we could use ipc, but an overkill for this app.
####################


logger = logging.getLogger(__name__)
logging.basicConfig(
    format="%(asctime)s | %(process)d | %(module)s : %(message)s", level=logging.INFO
)


def get_scheduled_jobs() -> [(str, str, str)]:
    scheduler_job_queue: Queue = container.scheduler_job_queue()
    redis = container.rds()

    registry = ScheduledJobRegistry(queue=scheduler_job_queue)
    jobs = Job.fetch_many(registry.get_job_ids(), connection=redis)
    return [(j.get_id(), j.get_status()) for j in jobs]


def clear_scheduler_queue():
    scheduler_job_queue: Queue = container.scheduler_job_queue()
    registry = ScheduledJobRegistry(queue=scheduler_job_queue)
    logger.info(f"Found jobs: {registry.get_job_ids()}; now removing them")
    for job_id in registry.get_job_ids():
        registry.remove(job_id, delete_job=True)
    scheduler_job_queue.empty()


def create_stat_collection_scheduler():
    scheduler_job_queue: Queue = container.scheduler_job_queue()
    config = container.cfg()

    # hack to make a schedule, actually a recursive enqueue
    # and we enqueue it early to reduce chance of failure
    scheduler_job_queue.enqueue_in(
        timedelta(minutes=config.scheduler_interval), create_stat_collection_scheduler
    )

    schedule = scheduler_job_queue.enqueue(_collect_all_video_stat)
    logger.info(
        f"_collect_all_video_stat job scheduled ; id: {schedule.get_id()} | status: {schedule.get_status()}"
    )

    # calculate performance a bit later than collecting video stats, it's not that necessary
    # we will always do the calculation later as we already got first and last view count
    schedule = scheduler_job_queue.enqueue_in(timedelta(minutes=config.scheduler_interval + 1), _calculate_all_video_performance)
    logger.info(
        f"_calculate_all_video_performance job scheduled ; id: {schedule.get_id()} | status: {schedule.get_status()}"
    )


def _collect_all_video_stat():
    video_repository: VideoRepository = container.video_repository()
    scheduler_job_queue: Queue = container.scheduler_job_queue()

    video_ids = video_repository.get_all_video_ids()
    logger.info(f"Schedule collecting stats of {len(video_ids)} videos")

    chunk_size = 50
    for x in range(0, len(video_ids), chunk_size):
        ids = video_ids[x: x + chunk_size]
        scheduler_job_queue.enqueue(_collect_video_list_stat, ids)


def _collect_video_list_stat(video_ids: List[str]):
    video_collector_service: VideoCollectorService = container.video_collector_service()
    video_repository: VideoRepository = container.video_repository()
    job_queue: Queue = container.job_queue()

    result = video_collector_service.collect_video_list_by_id_list(video_ids)
    for video in result.items:
        video_repository.save_video_stat(video.id, video.statistics)

        # may be candidate of a job
        video_repository.update_video_performance(
            video.id,
            int(video.statistics.viewCount) if video.statistics.viewCount else 0,
        )
    logger.info(f"Collected stat for video ids: {video_ids}")


def _calculate_all_video_performance():
    video_repository: VideoRepository = container.video_repository()

    video_repository.calculate_all_video_performance()
