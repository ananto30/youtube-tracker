import logging

from flask import Flask

from app.api import channel, video, scheduler
from app.api.swagger import swagger
from app.container.app_container import AppContainer

logger = logging.getLogger(__name__)
logging.basicConfig(
    format="%(asctime)s | %(process)d | %(module)s : %(message)s", level=logging.INFO
)


def create_app() -> Flask:
    container = AppContainer()
    container.config.from_yaml("config.yml", required=True)
    container.wire(modules=[channel, video])

    app = Flask("youtube-tracker")

    swagger.register(app)

    app.container = container
    app.register_blueprint(channel.channel_api)
    app.register_blueprint(video.video_api)
    app.register_blueprint(scheduler.scheduler_api)

    return app
