from app.data.response_data import ErrorResponse
from app.error.errors import ApplicationError


def get_error_response(err) -> (ErrorResponse, int):
    if isinstance(err, ApplicationError):
        return ErrorResponse(err=str(err)).dict(), err.status_code
    return ErrorResponse(err="Something went wrong, please check logs").dict(), 500
