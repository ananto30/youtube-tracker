import logging

from flask import Blueprint
from flask_pydantic import validate
from spectree import Response

from app.api import error_handler
from app.api.swagger import swagger
from app.data.request_data import SaveChannelRequest
from app.data.response_data import JobResponse, ErrorResponse
from app.error.errors import ApplicationError
from app.workers import rq_worker

channel_api = Blueprint(
    "channel_api",
    __name__,
)


@channel_api.post("/channel/collect")
@swagger.validate(
    json=SaveChannelRequest,
    resp=Response(HTTP_500=ErrorResponse, HTTP_200=JobResponse),
    tags=["Channel"],
)
@validate()
def collect_channel(body: SaveChannelRequest):
    """
    Collect channel videos

    (Async worker process)

    Creates a job that will scrape the channel and collect the videos with stats and calculate performance on schedule.
    If channel is already collected, new videos will be added and already existed ones will be ignored.
    """
    job_id, status = rq_worker.create_channel_video_job(body.channel_id)
    return JobResponse(status=status, job_id=job_id)


# handle errors
@channel_api.app_errorhandler(Exception)
def handle_500(err: Exception):
    logging.exception(err)
    return error_handler.get_error_response(err)
