import logging

from flask import Blueprint
from spectree import Response

from app.api import error_handler
from app.api.swagger import swagger
from app.data.response_data import ErrorResponse, ScheduledJobs, JobResponse
from app.error.errors import ApplicationError
from app.workers import stat_scheduler

scheduler_api = Blueprint(
    "scheduler_api",
    __name__,
)


@scheduler_api.get("/scheduler")
@swagger.validate(
    resp=Response(HTTP_500=ErrorResponse, HTTP_200=ScheduledJobs),
    tags=["Scheduler"],
)
def get_scheduler():
    """
    Get scheduled jobs

    List of the scheduled jobs. These are periodical jobs to calculate performance.
    """
    return ScheduledJobs(
        jobs=[
            JobResponse(job_id=id, status=status)
            for id, status in stat_scheduler.get_scheduled_jobs()
        ]
    ).dict()


@scheduler_api.post("/scheduler/start")
@swagger.validate(
    resp=Response(HTTP_500=ErrorResponse, HTTP_200=""),
    tags=["Scheduler"],
)
def start_scheduler():
    """
    Start scheduler

    Start the periodical scheduler that will calculate performance of the videos in db.
    """
    if len(stat_scheduler.get_scheduled_jobs()) == 0:
        stat_scheduler.create_stat_collection_scheduler()
    return ("", 200)


@scheduler_api.post("/scheduler/stop")
@swagger.validate(
    resp=Response(HTTP_500=ErrorResponse, HTTP_200=""),
    tags=["Scheduler"],
)
def stop_scheduler():
    """
    Stop scheduler

    Stop the periodical scheduler that calculates performance of the videos in db.
    """
    stat_scheduler.clear_scheduler_queue()
    return ("", 200)


# handle errors
@scheduler_api.app_errorhandler(Exception)
def handle_500(err: Exception):
    logging.exception(err)
    return error_handler.get_error_response(err)
