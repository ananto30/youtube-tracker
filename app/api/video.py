import logging

from dependency_injector.wiring import inject, Provide
from flask import Blueprint, request
from flask_pydantic import validate
from spectree import Response

from app.api import error_handler
from app.api.swagger import swagger
from app.container.app_container import AppContainer
from app.data.request_data import QueryVideo, SaveVideoRequest
from app.data.response_data import VideoResponseList, ErrorResponse, JobResponse
from app.error.errors import ApplicationError
from app.service.video import VideoService
from app.workers import rq_worker

video_api = Blueprint(
    "video_api",
    __name__,
)


@video_api.get("/videos")
@inject
@swagger.validate(
    query=QueryVideo,
    resp=Response(HTTP_500=ErrorResponse, HTTP_200=VideoResponseList),
    tags=["Video"],
)
def query_video(video_service: VideoService = Provide[AppContainer.video_service]):
    """
    Query videos

    Filter by tags or video performance.

    For multiple tags use comma separated value. Like `/videos?tag=vlog,food`

    `performance_gte` = Filter by performance greater than or equal to

    `performance_lte` = Filter by performance less than or equal to


    **Results are sorted by performance descending.**
    """
    query = QueryVideo.parse_obj(request.args)
    videos = video_service.get_videos(query)
    return VideoResponseList(videos=videos).dict()


@video_api.post("/video/collect")
@swagger.validate(
    json=SaveVideoRequest,
    resp=Response(HTTP_500=ErrorResponse, HTTP_200=JobResponse),
    tags=["Video"],
)
@validate()
def collect_video(body: SaveVideoRequest):
    """
    Collect video and save stats

    (Async worker process)

    Creates a job that will collect a single video's stats and calculate performance on schedule.
    """
    job_id, status = rq_worker.create_single_video_job(body.video_id)
    return JobResponse(job_id=job_id, status=status)


@video_api.post("/video/collect_stat")
@swagger.validate(
    json=SaveVideoRequest,
    resp=Response(HTTP_500=ErrorResponse, HTTP_200=JobResponse),
    tags=["Video"],
)
@validate()
def collect_video_stat(body: SaveVideoRequest):
    """
    Collect video stat

    (Async worker process)

    Collect **only stats** of a video by a video id. Even if the video is not listed earlier.

    **THIS API SHOULD NOT BE USED WITHOUT NECESSITY.**
    """
    job_id, status = rq_worker.create_single_video_stat_collection_job(body.video_id)
    return JobResponse(job_id=job_id, status=status)


# handle errors
@video_api.app_errorhandler(Exception)
def handle_500(err: Exception):
    logging.exception(err)
    return error_handler.get_error_response(err)
