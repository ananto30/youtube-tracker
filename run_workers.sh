#!/bin/bash
export $(grep -v '^#' .env | xargs)
# naive way to run two workers, use supervisord like process manager in prod
# for more info https://python-rq.org/patterns/supervisor/
rq worker --with-scheduler --url redis://localhost:6379 default scheduler &
rq worker --with-scheduler --url redis://localhost:6379 default scheduler  &
wait